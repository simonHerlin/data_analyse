import matplotlib.pyplot as plt
from datetime import datetime
from utils.Recover_data import read
from pattern import build_patterns
from pattern import get_pattern_infos
from pattern import build_cluster
from pattern import select_patterns_with_size
from pattern import build_sub_patterns


def plot_debit_histogram(clusters, values):
    for index in range(0, len(clusters)):
        clusters[index] = sorted(clusters[index], key=lambda c: c[0])
        selected_values = []
        for k in range(0, len(clusters[index])):
            for r in range(0, len(clusters[index][k])):
                selected_values.append(values[clusters[index][k][r], 0])
        plt.subplot(len(clusters), 1, index + 1)
        plt.hist(x=selected_values, bins=max(selected_values), facecolor='gray', density=True)
    plt.show()


def plot_volume_histogram(clusters, values):
    for index in range(0, len(clusters)):
        clusters[index] = sorted(clusters[index], key=lambda c: c[0])
        selected_values = []
        for k in range(0, len(clusters[index])):
            for r in range(0, len(clusters[index][k])):
                selected_values.append(values[clusters[index][k][r], 1])
        plt.subplot(len(clusters), 1, index + 1)
        plt.hist(x=selected_values, bins=max(selected_values), facecolor='gray', density=True)
    plt.show()


def debit(pattern, values):
    print(datetime.fromtimestamp(values[pattern[0]][0, 3]), get_pattern_infos(values, pattern, len(pattern)))


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'

    values, max_values, days = read(file)
    patterns, max_length = build_patterns(values, 30 * 60)

    pattern_length = 2
    all_patterns = select_patterns_with_size(patterns, pattern_length)
    for k in range(pattern_length, max_length + 1):
        selected_patterns = select_patterns_with_size(patterns, k)
        sub_patterns = build_sub_patterns(selected_patterns, pattern_length)
        all_patterns += sub_patterns

    clusters = build_cluster(all_patterns, values, 250)
    plt.figure(1)
    plot_debit_histogram(clusters, values)
    plt.figure(2)
    plot_volume_histogram(clusters, values)


if __name__ == "__main__":
    main()
