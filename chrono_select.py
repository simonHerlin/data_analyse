from chrono import plot_chronology
import matplotlib.pyplot as plt
import numpy as np
from utils.Recover_data import read
from utils.GMM import select, build


def select_values(values, components, component_index):
    # TODO: improve with numpy API
    new_values = []
    for i in range(np.size(values, 0)):
        if components[i] == component_index:
            v = []
            for k in range(0, 4):
                v.append(values[i, k])
            new_values.append(v)
    return np.matrix(new_values, dtype=np.int32)


def select_values_multi(values, components, component_indexes):
    # TODO: improve with numpy API
    new_values = []
    for i in range(np.size(values, 0)):
        if components[i] in component_indexes:
            v = []
            for k in range(0, 4):
                v.append(values[i, k])
            new_values.append(v)
    return np.matrix(new_values, dtype=np.int32)


def plot_selected_chrono(values, days, components, component_number):
    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']

    for k in range(0, component_number):
        selected_values = select_values(values, components, k)
        if np.size(selected_values, 0) > 0:
            plot_chronology(selected_values, days, 0, 28, colors[k], True)


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '3'
    component_number = 8

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    gmm = build(values, component_number)
    components = select(values, gmm, component_number)

    for k in range(0, component_number):
        plt.figure(k + 1)
        selected_values = select_values(values, components, k)
        plot_chronology(selected_values, days, 0, 28)

    plt.figure(component_number + 1)
    plot_selected_chrono(values, days, components, component_number)

    plt.show()


if __name__ == "__main__":
    main()
