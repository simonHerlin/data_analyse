import numpy as np
import matplotlib.pyplot as plt
from utils.Recover_data import read
from distance import compute_distance
from utils.GMM import select, build


def select_data_of_component(values, components, component_index):
    # TODO: improve with numpy API
    new_values = []
    for i in range(np.size(values, 0)):
        if components[i] == component_index:
            v = []
            for k in range(0, 4):
                v.append(values[i, k])
            new_values.append(v)
    return np.matrix(new_values, dtype=np.int32)


def select_closed_data(values, distances, reference):
    # TODO: improve with numpy API
    new_values = []
    new_distances = []
    for i in range(np.size(values, 0)):
        if distances[i] < reference:
            v = []
            for k in range(0, 4):
                v.append(values[i, k])
            new_values.append(v)
            new_distances.append(distances[i])
    return np.matrix(new_values, dtype=np.int32), new_distances


def compute_distances(values):
    distances = []
    for i in range(0, np.size(values, 0)):
        distance = compute_distance(values, i)
        distances.append(distance)
    return distances, max(distances)


def plot(debits, distances, color='black', label=''):
    plt.scatter(debits, distances, c=color, label=label)


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'
    component_number = 5

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 19.2  # width = 1920 pixels
    fig_size[1] = 19.2  # height = 1920 pixels

    values, max_values, days = read(file)
    distances, max_distance = compute_distances(values)
    gmm = build(values, component_number)
    components = select(values, gmm, component_number)

    plt.figure(1)
    plot(np.array(values[:, 0]), distances)

    plt.figure(2)
    plot(np.array(values[:, 1]), distances)

    plt.figure(3)
    plot(np.array(values[:, 0]), np.array(values[:, 1]))

    plt.figure(4)
    new_values, new_distances = select_closed_data(values, distances, 15 * 60)
    plot(np.array(new_values[:, 0]), new_distances)

    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']
    plt.figure(5)
    for k in range(0, component_number):
        selected_values_k = select_data_of_component(values, components, k)
        new_distances_k, max_distance_k = compute_distances(selected_values_k)
        new_values_k, new_distances_k = select_closed_data(selected_values_k, new_distances_k, 15 * 60)
        if np.size(new_distances_k, 0) > 0:
            plot(np.array(new_values_k[:, 0]), new_distances_k, colors[k], str(gmm.means_[k][0]))
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
