import math
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from utils.Recover_data import read
from utils.GMM import build, select


def compute_threshold(values, max_value):
    hist, edges = np.histogram(values, bins=int(max_value / 5), density=True)
    index = len(hist) - 1
    while index >= 0 and hist[index] < 1e-3:
        index -= 1
    if index >= 0:
        return edges[index]
    else:
        return edges[-1]


def plot_debit_histogram(values, max_value, component_number):
    plt.subplot(component_number + 1, 1, 1)
    plt.title("global")
    plt.hist(x=values, bins=int(max_value / 5), facecolor='gray', density=True)


def plot_gaussian_distributions(gmm, max_values, component_number):
    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']

    x = np.linspace(0, max_values[0], max_values[0])
    for i in range(0, component_number):
        plt.plot(x, gmm.weights_[i] * stats.norm.pdf(x, gmm.means_[i], math.sqrt(gmm.covariances_[i])), c=colors[i])


def plot_volumes(values, gmm, components, component_number):
    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']

    for k in range(0, component_number):
        Z = []
        Max = 1
        Min = 100000
        Sum = 0
        N = 0

        # select
        for i in range(np.size(values, 0)):
            if components[i] == k:
                value = values[i, 1]
                Z.append(value)
                Sum += value
                N += 1
                if Max < value:
                    Max = value
                if Min > value:
                    Min = value

        # plot histogramme des volumes de la kème gaussienne
        plt.subplot(component_number + 1, 1, k + 2)
        plt.title("volume - débit moyen = " + str(gmm.means_[k][0]))
        n, bins, patches = plt.hist(x=Z, bins=int(Max), facecolor=colors[k], density=True)

        # calcul de la loi de Poisson
        if N > 0:
            x = np.array(range(int(Min), int(Max + 1)))
            mu = Sum / N - Min
            plt.plot(x, stats.poisson.pmf(x - Min, mu), c="black")


def filter_values(values, max_debit):
    # TODO: improve with numpy API
    new_values = []
    for i in range(np.size(values, 0)):
        if values[i, 0] < max_debit:
            v = []
            for k in range(0, 4):
                v.append(values[i, k])
            new_values.append(v)
    return np.matrix(new_values, dtype=np.int32)


def plot_all(values, max_values, component_number, figure_index):
    plt.figure(figure_index + 1)
    plot_debit_histogram(values[:, 0], max_values[0], component_number)

    gmm = build(values, component_number)
    components = select(values, gmm, component_number)

    plot_gaussian_distributions(gmm, max_values, component_number)
    plot_volumes(values, gmm, components, component_number)


def plot_filtered_values(values, max_values, component_number, figure_index):
    plt.figure(figure_index + 1)
    max_debit = compute_threshold(values[:, 0], max_values[0])

    filtered_values = filter_values(values, max_debit)

    plt.figure(figure_index + 2)
    plot_debit_histogram(filtered_values[:, 0], max_values[0], component_number)

    gmm = build(filtered_values, component_number)
    components = select(filtered_values, gmm, component_number)

    plot_gaussian_distributions(gmm, max_values, component_number)
    plot_volumes(filtered_values, gmm, components, component_number)


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'
    component_number = 8

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 19.2  # width = 1920 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)

    plot_all(values, max_values, component_number, 0)
    plot_filtered_values(values, max_values, component_number, 1)

    plt.show()


if __name__ == "__main__":
    main()
