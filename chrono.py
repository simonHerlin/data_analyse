import datetime
import matplotlib.pyplot as plt
import numpy as np
from utils.Recover_data import read


def select(values, day, index):
    begin = int(datetime.datetime.fromisoformat(day + ' 00:00:00').timestamp())
    end = begin + 24 * 60 * 60
    while index < np.size(values, 0) and values[index, 3] < begin:
        index += 1
    new_values = np.zeros((24 * 60 * 60,), dtype=int)
    while index < np.size(values, 0) and values[index, 3] < end:
        if index < np.size(values, 0) - 1 and values[index + 1, 4] == 1 and \
                values[index, 3] > values[index + 1, 3] - values[index + 1, 2]:
            finish = values[index + 1, 3] - values[index + 1, 2]
        else:
            finish = values[index, 3]
        for t in range(values[index, 3] - values[index, 2], finish):
            if t < end:
                new_values[t - begin] += values[index, 1]
        index += 1
    max_value = new_values.max()
    return new_values, max_value, index


def select_close(values, day, close, index):
    begin = int(datetime.datetime.fromisoformat(day + ' 00:00:00').timestamp())
    end = begin + 24 * 60 * 60
    while index < np.size(values, 0) and values[index, 3] < begin:
        index += 1
    new_values = np.zeros((24 * 60 * 60,), dtype=int)
    while index < np.size(values, 0) and values[index, 3] < end:
        if close[index]:
            if index < np.size(values, 0) - 1 and values[index + 1, 4] == 1 and \
                    values[index, 3] > values[index + 1, 3] - values[index + 1, 2]:
                finish = values[index + 1, 3] - values[index + 1, 2]
            else:
                finish = values[index, 3]
            for t in range(values[index, 3] - values[index, 2], finish):
                if t < end:
                    new_values[t - begin] += values[index, 1]
        index += 1
    max_value = new_values.max()
    return new_values, max_value, index


def plot_chronology(values, days, begin, end, color='black', max=True):
    index = 0
    for k in range(begin, end + 1):
        new_values, max_value, index = select(values, days[k], index)
        plt.subplot(end - begin + 1, 1, k - begin + 1)
        plt.title(days[k])
        if max and max_value > 0:
            plt.axis([0, len(new_values), 0, max_value + 1])
            plt.plot(new_values, linewidth=1, color=color)


def plot_close_chronology(values, days, begin, end, close, color='black', max=True):
    index = 0
    for k in range(begin, end + 1):
        new_values, max_value, index = select_close(values, days[k], close, index)
        plt.subplot(end - begin + 1, 1, k - begin + 1)
        plt.title(days[k])
        if max and max_value > 0:
            plt.axis([0, len(new_values), 0, max_value + 1])
            plt.plot(new_values, color=color)


def detect_close(values, distance):
    close = []
    for i in range(0, np.size(values, 0)):
        if i == 0:
            close.append(values[i + 1, 3] - values[i, 3] < distance)
        else:
            if i == np.size(values, 0) - 1:
                close.append(values[i, 3] - values[i - 1, 3] < distance)
            else:
                close.append(values[i + 1, 3] - values[i, 3] < distance or values[i, 3] - values[i - 1, 3] < distance)
    return close


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)

    plt.figure(1)
    plot_chronology(values, days, 0, 3)

    plt.figure(2)
    plot_close_chronology(values, days, 0, 28, detect_close(values, 900))

    plt.show()


if __name__ == "__main__":
    main()
