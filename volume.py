import matplotlib.pyplot as plt
from utils.Recover_data import read


def plot_volumes(values, max_values):
    plt.title("volume")
    n, bins, patches = plt.hist(x=values[:, 1], bins=int(max_values[1]))


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'
    component_number = 6

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 19.2  # width = 1920 pixels
    fig_size[1] = 6.4  # height = 640 pixels

    values, max_values, days = read(file)

    plot_volumes(values, max_values)

    plt.show()


if __name__ == "__main__":
    main()
