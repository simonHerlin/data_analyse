from utils.Recover_data import read
import numpy as np
from chrono import plot_chronology
from pattern import build_patterns, build_cluster
from category import merge_sub_clusters
import matplotlib.pyplot as plt
import operator

# def regroup_volume_debit(values):
#     result = []
#     first = True
#     print ("nombre value with way = 0 :", len(values))
#     for v in values:
#         find = False
#         if v[4] != 0:
#             if first:
#                 result.append([v[0], v[1], v[2], v[3], v[4], 0])
#                 # r.append([v[0], v[1], 0])
#                 first = False
#             for i in range (1, len(result)):
#                 if find is False and v[0] == result[i][0] and v[1] == result[i][1] and v[2] == result[i][2]:
#                     result[i][5] += 1
#                     # r[i][2] += 1
#                     find = True
#             if find is False:
#                 result.append([v[0], v[1], v[2], v[3], v[4], 0])
#                 # r.append([v[0], v[1], 0])
#     print("nomber value with différent volume, debit : ", len(result))
#
#     r = []
#     for i in range(len(result)):
#         if result[i][1] >= 4 and result[i][1] <= 12 and result[i][5] > 10:
#             r.append([result[i][1], result[i][5]])
#
#     import operator
#     npr = np.array(r)
#     index, value = max(enumerate(npr[:, 1]), key=operator.itemgetter(1))
#
#     print(r)
#     print(index)
#     print(value)
#
#     wc = r[index][0]
#     print(wc)
#
#     X = np.array(result)
#     print(max(X[:, 4]))


def regroup(values):
    result = []
    first = True
    for j in range(len(values)):
        find = False
        if values[j][1] >= 3 and values[j][1] <= 12:
            if first:
                result.append([values[j][0], values[j][1], values[j][2], values[j][3], 1])
                first = False
            for i in range (1, len(result)):
                # if find is False and (values[j][1] == result[i][1] and abs(values[j][0] - result[i][0]) <= 30):
                if find is False and (values[j][1] == result[i][1] and values[j][0] == result[i][0]):
                    result[i][4] += 1
                    find = True
            if find is False:
                result.append([values[j][0], values[j][1], values[j][2], values[j][3], 1])
    return result


def find_index(values):
    # Find the most recurring
    index1, value = max(enumerate(values[:, 4]), key=operator.itemgetter(1))

    # find the second most recurring
    sort = np.sort(values[:, 4])
    x = sort[-2]
    print(x)
    index2 = int(np.where(values[:, 4] == sort[-2])[0])
    return index1, index2


def no_label_regroup(values, labels):
    result = regroup(values)
    # print("#######")
    # print(result)
    # print("#########")
    diff = np.array(result)
    # print("Nomber diff")
    # print(diff[:, 4])

    index, value = max(enumerate(diff[:, 4]), key=operator.itemgetter(1))

    select_value = []
    value_label = max(np.array(labels)) + 1

    for i in range(len(values)):
        if values[i][1] == diff[index][1] and values[i][0] < 380 and values[i][0] > 250:
            select_value.append(values[i])
            labels[i] = value_label + 1
    return select_value, labels

    # old
    # index1, index2 = find_index(diff)

    # print("index1: ", diff[index1])
    # print("index2: ", diff[index2])

    # select_value = []
    # value_label = max(np.array(labels))

    # for i in range(len(values)):
    #     if values[i][1] == diff[index1][1] and abs(values[i][0] - diff[index1][0]) <= 30 \
    #             or values[i][1] == diff[index2][1] and abs(values[i][0] - diff[index2][0]) <= 30:
    #         select_value.append(values[i])
    #         labels[i] = value_label + 1
    # return select_value, labels


def select_no_label(values, labels):
    selected_values = []
    for index in range(len(values)):
        if labels[index] == 0:
            v = []
            for p in range(0, 5):
                v.append(values[index, p])
            selected_values.append(v)
    return selected_values


def select_with_label(values, labels):
    select_value = []
    # print("##### value in select #####")
    # print(values)
    # print("##### value in select #####")
    for index in range(len(values)):
        if labels[index] != 0:
            v = []
            for p in range(0, 5):
                v.append(values[index, p])
            select_value.append(v)
    return select_value


def plot_data_no_label(values):
    plt.scatter(values[:, 2], values[:, 1])
    plt.show()


def plot_lables(labels, values, days):
    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']
    for index in range(len(labels)):
        selected_values = []
        for p in range(0, 5):
            selected_values.append(values[index, p])
        plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 0, 28, colors[labels[index]])
    plt.show()


# def plot_labelled_cluster_with_wc(values, labels, days):
#     selected_values = []
#     for index in range(0, len(values)):
#         v = []
#         for p in range(0, 5):
#             v.append(values[index, p])
#         selected_values.append(v)
#     plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 0, 28, 'black')
#
#
#         # if labels[index] == size:
#         #     v = []
#         #     for p in range(0, 5):
#         #         v.append(values[index, p])
#         #     selected_values.append(v)
#     # plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 0, 28, 'black')
#     plt.show()


# def test(values, labels):
#     select_value = []
#     for index in range(len(values)):
#         if labels[index] == 0 or labels[index] == 2:
#             v = []
#             for p in range(0, 5):
#                 v.append(values[index, p])
#             select_value.append(v)
#     return select_value

def get_labels(values, max_length, patterns):
    labels = [0] * len(values)
    thresholds = [100, 20, 20, 10, 10, 10, 10, 10]
    for k in range(max_length, 1, -1):
        all_patterns = merge_sub_clusters(patterns, k, max_length)
        clusters = build_cluster(all_patterns, values, thresholds[k - 2])
        for index in range(0, len(clusters)):
            for p in range(0, len(clusters[index])):
                for r in range(0, len(clusters[index][p])):
                    if labels[clusters[index][p][r]] == 0:
                        labels[clusters[index][p][r]] = k
    return labels


def main():
    file = '23'

    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    patterns, max_length = build_patterns(values, 30 * 60)

    labels = get_labels(values, max_length, patterns)
    values_no_labels = select_no_label(values, labels)

    # display all data
    # plt.figure("all data")
    # plot_chronology(np.matrix(values, dtype=np.int32), days, 0, 28)
    # plt.show()

    ## data wit no label
    # plt.figure("No label before work")
    # plot_chronology(np.matrix(values_no_labels, dtype=np.int32), days, 0, 28)
    # plt.show()

    # display data with label before work
    # values_with_label = select_with_label(values, labels)
    # plot_chronology(np.matrix(values_with_label, dtype=np.int32), days, 0, 28)
    # plt.show()

    ## find WC ?
    select_values, labels = no_label_regroup(values_no_labels, labels)
    plt.figure("new label wc")
    plot_chronology(np.matrix(select_values, dtype=np.int32), days, 0, 28)
    plt.show()

    ## all data with label after work
    # values_with_label = select_with_label(values, labels)
    # plt.figure("all data with label")
    # plot_chronology(np.matrix(values_with_label, dtype=np.int32), days, 0, 28)
    # plt.show()

    ## all data with no label after work
    # plt.figure("all data with no label after work")
    # values_no_labels = select_no_label(values, labels)
    # plot_chronology(np.matrix(values_no_labels, dtype=np.int32), days, 0, 28)
    # plt.show()

    # test
    # values_test = test(values, labels)
    # plot_chronology(np.matrix(values_test, dtype=np.int32), days, 0, 28)
    # plt.show()


    # plt.show()
    # plot_lables(labels, values, days)


if __name__ == "__main__":
    main()
