import collections
import numpy as np
from utils.Recover_data import read
from scipy.cluster.hierarchy import fclusterdata
import matplotlib.pyplot as plt
from chrono import plot_chronology
import itertools


def compute_distance(values, i):
    if i == 0:
        return -1, values[i + 1, 3] - values[i, 3]
    else:
        if i == np.size(values, 0) - 1:
            return values[i, 3] - values[i - 1, 3], -1
        else:
            return values[i, 3] - values[i - 1, 3], values[i + 1, 3] - values[i, 3]


def compute_finish(values, i):
    if i < np.size(values, 0) - 1 and values[i + 1, 4] == 1 and values[i, 3] > values[i + 1, 3] - values[i + 1, 2]:
        finish = values[i + 1, 3] - values[i + 1, 2]
    else:
        finish = values[i, 3]
    return finish


def compute_overlaps(values, i):
    finish_i = compute_finish(values, i)
    finish_i_1 = compute_finish(values, i - 1)
    if i == 0:
        return False, finish_i > values[i + 1, 3]
    else:
        if i == np.size(values, 0) - 1:
            return finish_i_1 > values[i, 3], False
        else:
            return finish_i_1 > values[i, 3], finish_i > values[i + 1, 3]


def build_patterns(values, reference):
    max_length = 0
    patterns = []
    current = [[0]]
    for i in range(1, np.size(values, 0)):
        before_distance, after_distance = compute_distance(values, i)
        before_overlap, after_overlap = compute_overlaps(values, i)
        if 0 <= before_distance <= reference:
            if before_overlap:
                for k in range(0, len(current)):
                    v = []
                    for r in range(0, len(current[k]) - 1):
                        v.append(current[k][r])
                    v.append(i)
                    current.append(v)
            else:
                for k in range(0, len(current)):
                    current[k].append(i)
        else:
            if len(current) > 0:
                for k in range(0, len(current)):
                    if len(current[k]) > 1:
                        patterns.append(current[k])
                        if max_length < len(current[k]):
                            max_length = len(current[k])
                current = [[i]]
    return patterns, max_length


def get_pattern_infos(values, pattern, size):
    durations = []
    debits = []
    volumes = []
    for i in range(0, size - 1):
        durations.append(values[pattern[i + 1], 3] - values[pattern[i], 3])
    for i in range(0, size):
        debits.append(values[pattern[i], 0])
    for i in range(0, size):
        volumes.append(values[pattern[i], 1])
    return durations, debits, volumes


def compute_distance_pattern(first, second):
    n = int((len(first) + 1) / 3)
    diff_durations = 0
    for i in range(0, n - 1):
        if abs((first[0 + i] - second[0 + i])) > 60:
            if float(first[0 + i] + second[0 + i]) / 2 != 0:
                diff_durations += abs((first[0 + i] - second[0 + i]) / (float(first[0 + i] + second[0 + i]) / 2))
            else:
                diff_durations += abs(first[0 + i] - second[0 + i])
    diff_debits = 0
    for i in range(0, n):
        if float(first[1 + i] + second[1 + i]) / 2 != 0:
            diff_debits += abs((first[1 + i] - second[1 + i]) / (float(first[1 + i] + second[1 + i]) / 2))
        else:
            diff_debits += abs((first[1 + i] - second[1 + i]))
    diff_volumes = 0
    for i in range(0, n):
        if float(first[2 + i] + second[2 + i]) / 2 != 0:
            diff_volumes += abs((first[2 + i] - second[2 + i]) / (float(first[2 + i] + second[2 + i]) / 2))
        else:
            diff_volumes += abs((first[2 + i] - second[2 + i]))
    return (diff_durations + diff_debits + diff_volumes) / 3


def is_different(p1, p2, size):
    ok = True
    i1 = 0
    while i1 < size and ok:
        i2 = 0
        while i2 < size and ok:
            ok = p1[i1] != p2[i2]
            i2 += 1
        i1 += 1
    return ok


def build_cluster(patterns, values, size):
    result = []
    list = []
    p = []
    for k in range(0, len(patterns)):
        durations, debits, volumes = get_pattern_infos(values, patterns[k], len(patterns[k]))
        list.append(durations + debits + volumes)
        p.append(patterns[k])
    clusters = fclusterdata(list, t=0.06, criterion='distance', metric=compute_distance_pattern)
    cluster_number = max(clusters)
    for k in range(0, cluster_number):
        if collections.Counter(clusters)[k] > size:
            result.append([])
            for i in range(0, len(list)):
                if clusters[i] == k:
                    result[len(result) - 1].append(p[i])
    return result


def select_patterns_with_size(patterns, size):
    selected_patterns = []
    for k in range(0, len(patterns)):
        if len(patterns[k]) == size:
            selected_patterns.append(patterns[k])
    return selected_patterns


def build_sub_patterns(patterns, n):
    sub_patterns = []
    for k in range(0, len(patterns)):
        for it in itertools.combinations(patterns[k], n):
            sub_patterns.append(list(it))
    return sub_patterns


def plot_patterns(clusters, values, days):
    colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'brown', 'gray']
    for index in range(0, len(clusters)):
        clusters[index] = sorted(clusters[index], key=lambda c: c[0])
        selected_values = []
        for k in range(0, len(clusters[index])):
            for r in range(0, len(clusters[index][k])):
                v = []
                for p in range(0, 5):
                    v.append(values[clusters[index][k][r], p])
                selected_values.append(v)
        plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 26, 26, colors[index])

    plt.show()


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    patterns, max_length = build_patterns(values, 30 * 60)

    pattern_length = 2
    all_patterns = select_patterns_with_size(patterns, pattern_length)
    for k in range(pattern_length, max_length + 1):
        selected_patterns = select_patterns_with_size(patterns, k)
        sub_patterns = build_sub_patterns(selected_patterns, pattern_length)
        all_patterns += sub_patterns

    clusters = build_cluster(all_patterns, values, 100)

    for i in range(0, len(clusters)):
        print(i, clusters[i][0], len(clusters[i]))


    plot_patterns(clusters, values, days)
    return values, clusters, days


if __name__ == "__main__":
    main()
