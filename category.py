import numpy as np
from utils.Recover_data import read
from pattern import build_patterns
from pattern import get_pattern_infos
from pattern import build_cluster
from pattern import select_patterns_with_size
from pattern import build_sub_patterns
import matplotlib.pyplot as plt
from chrono import plot_chronology


def merge_sub_clusters(patterns, pattern_length, max_length):
    all_patterns = select_patterns_with_size(patterns, pattern_length)
    for k in range(pattern_length, max_length + 1):
        selected_patterns = select_patterns_with_size(patterns, k)
        sub_patterns = build_sub_patterns(selected_patterns, pattern_length)
        all_patterns += sub_patterns
    return all_patterns


def plot_no_labelled(values, labels, days):
    selected_values = []
    for index in range(0, len(values)):
        if labels[index] == 0:
            v = []
            for p in range(0, 5):
                v.append(values[index, p])
            selected_values.append(v)
    plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 0, 28, 'black')
    plt.show()


def plot_labelled_cluster(values, labels, days, size):
    selected_values = []
    for index in range(0, len(values)):
        if labels[index] == size:
            v = []
            for p in range(0, 5):
                v.append(values[index, p])
            selected_values.append(v)
    plot_chronology(np.matrix(selected_values, dtype=np.int32), days, 0, 28, 'black')
    plt.show()


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    patterns, max_length = build_patterns(values, 30 * 60)

    labels = [0] * len(values)
    thresholds = [100, 20, 20, 10, 10, 10, 10, 10]
    for k in range(max_length, 1, -1):
        all_patterns = merge_sub_clusters(patterns, k, max_length)
        clusters = build_cluster(all_patterns, values, thresholds[k - 2])
        for index in range(0, len(clusters)):
            for p in range(0, len(clusters[index])):
                for r in range(0, len(clusters[index][p])):
                    if labels[clusters[index][p][r]] == 0:
                        labels[clusters[index][p][r]] = k
        print(k, (100. * labels.count(k)) / len(labels))
    print((100. * labels.count(0)) / len(labels))

    # import operator
    # diff = np.array(labels)
    # index, value = max(enumerate(diff), key=operator.itemgetter(1))
    #
    #
    # print(value)

    plt.figure(1)
    plot_no_labelled(values, labels, days)
    plt.figure(2)
    plot_labelled_cluster(values, labels, days, 4)
    plt.figure(3)
    plot_labelled_cluster(values, labels, days, 3)
    plt.figure(4)
    plot_labelled_cluster(values, labels, days, 2)


if __name__ == "__main__":
    main()
