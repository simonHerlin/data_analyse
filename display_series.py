import matplotlib.pyplot as plt
from utils.Recover_data import read


def plot(values, max_values):
    for i in range(0, 3):
        plt.subplot(3, 1, i + 1)
        plt.title("global")
        if max_values[i] > 100:
            n, bins, patches = plt.hist(x=values[:, i], bins=int(max_values[i] / 5), facecolor='gray', density=True)
        else:
            n, bins, patches = plt.hist(x=values[:, i], bins=int(max_values[i]), facecolor='gray', density=True)


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '23'

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 19.2  # width = 1920 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    plot(values, max_values)
    plt.show()


if __name__ == "__main__":
    main()
