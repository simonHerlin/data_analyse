import csv
import datetime
import math
import numpy as np


def read(file):
    data = []
    max_values = [1, 1, 1, 1]
    days = []
    with open('data/' + file + '.csv', 'r') as csvfile:
        first = True
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            if not first:
                if int(row[3]) > 0 and int(row[4]) > 0 and (int(row[5]) == 0 or int(row[5]) == 1):
                    values = [int(row[3]), int(row[4]), (float(row[4]) / float(row[3])) * 3600]
                    day = row[6][1:-1].split(' ')[0]
                    if len(days) == 0 or days[-1] != day:
                        days.append(day)
                    date = datetime.datetime.fromisoformat(row[6][1:-1])
                    t_global = int(date.timestamp())
                    values.append(t_global)
                    values.append(int(row[5]))
                    # [ debit, volume, duration, date/time, way ]
                    data.append(values)
                    for i in range(0, 4):
                        if max_values[i] < math.ceil(values[i]):
                            max_values[i] = math.ceil(values[i])
            else:
                first = False
    days.sort()
    values = np.matrix(data, dtype=np.int32)
    values = np.sort(
        values.view(dtype=[('debit', np.int32),
                           ('volume', np.int32),
                           ('duration', np.int32),
                           ('time', np.int32),
                           ('way', np.int32)]),
        order=['time', 'way'],
        axis=0).view(np.int32)
    return values, max_values, days
