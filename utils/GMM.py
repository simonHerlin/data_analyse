import math
import numpy as np
import scipy.stats as stats
from sklearn import mixture


def build(values, component_number):
    samples = np.array(values[:, 0]).reshape(-1, 1)
    g = mixture.GaussianMixture(n_components=component_number)
    gmm = g.fit(samples)
    return gmm


def select(values, gmm, component_number):
    components = []
    for i in range(np.size(values, 0)):
        value_max = 0
        k_max = -1
        for k in range(0, component_number):
            p = gmm.weights_[k] * stats.norm.pdf(values[i, 0], gmm.means_[k], math.sqrt(gmm.covariances_[k]))
            if value_max < p:
                value_max = p
                k_max = k
        components.append(k_max)
    return components
