from xml.etree import ElementTree
tree = ElementTree.parse('LV_2.xml')
root = tree.getroot().find('Data').find('FlowRecords')

for att in root:
    flow = att.find('Flow').text
    volume = att.find('Volume').text
    way = att.find('Way').text
    date = att.find('Date').text
    print(',,,' + flow + ',' + volume + ',' + way + ',' + date + ',')
