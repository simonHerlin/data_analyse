import numpy as np
import matplotlib.pyplot as plt
from utils.GMM import select, build
from utils.Recover_data import read
from chrono_select import select_values
from chrono_select import select_values_multi
from chrono import plot_chronology


def plot_one(distances, max_distance, k, gmm, component_number):
    plt.subplot(component_number, 1, k + 1)
    plt.title("débit moyen = " + str(gmm.means_[k][0]))
    plt.hist(x=distances, bins=int(max_distance), facecolor='gray')


def plot_two(distances, max_distance, k1, k2, gmm, component_number):
    plt.subplot(component_number * component_number, 1, k1 + k2 * component_number)
    plt.title("débit moyen = " + str(gmm.means_[k1][0]) + " " + str(gmm.means_[k2][0]))
    plt.hist(x=distances, bins=int(max_distance), facecolor='gray')


def plot_global(distances, max_distance):
    plt.title("global")
    plt.hist(x=distances, bins=int(max_distance), facecolor='gray')


def compute_distance(values, i):
    if i == 0:
        distance = values[i + 1, 3] - values[i, 3]
    else:
        if i == np.size(values, 0) - 1:
            distance = values[i, 3] - values[i - 1, 3]
        else:
            distance = min(values[i + 1, 3] - values[i, 3],
                           values[i, 3] - values[i - 1, 3])
    return distance


def compute_distance_one(values, gmm, components, component_number, plot_callback):
    for k in range(0, component_number):
        selected_values = select_values(values, components, k)
        if np.size(selected_values, 0) > 0:
            distances = []
            max_distance = 0
            for i in range(0, np.size(selected_values, 0)):
                distance = compute_distance(selected_values, i)
                if distance < 15 * 60:  # if < 15 minutes
                    if max_distance < distance:
                        max_distance = distance
                    distances.append(distance)
            plot_callback(distances, max_distance, k, gmm, component_number)


def compute_distance_two(values, gmm, components, component_number, plot_callback):
    for k1 in range(0, component_number):
        for k2 in range(0, component_number):
            if k1 != k2:
                selected_values = select_values_multi(values, components, [k1, k2])
                if np.size(selected_values, 0) > 0:
                    distances = []
                    max_distance = 0
                    for i in range(0, np.size(selected_values, 0)):
                        distance = compute_distance(selected_values, i)
                        if distance < 15 * 60:  # if < 15 minutes
                            if max_distance < distance:
                                max_distance = distance
                            distances.append(distance)
                    if len(distances) > 0:
                        plot_callback(distances, max_distance, k1, k2, gmm, component_number)


def compute_distance_global(values, plot_callback):
    distances = []
    max_distance = 0
    for i in range(0, np.size(values, 0)):
        distance = compute_distance(values, i)
        if distance < 15 * 60:  # if < 15 minutes
            if max_distance < distance:
                max_distance = distance
            distances.append(distance)
    plot_callback(distances, max_distance)


def compute_distance_histogram(values):
    distances = []
    for i in range(0, np.size(values, 0)):
        distance = compute_distance(values, i)
        if distance < 30 * 60:  # if < 15 minutes
            distances.append(distance)
    if len(distances) > 0:
        bins = max(distances) - min(distances) + 1
        if bins < 1:
            bins = 1
    else:
        bins = 1
    hist, edges = np.histogram(distances, bins=bins)
    return hist, edges


def select_values_with_distance(values, reference):
    selected_values = []
    for i in range(0, np.size(values, 0) - 1):
        distance = values[i + 1, 3] - values[i, 3]
        if abs(distance - reference) < 120:
            v1 = []
            v2 = []
            for p in range(0, 4):
                v1.append(values[i, p])
                v2.append(values[i + 1, p])
            selected_values.append(v1)
            selected_values.append(v2)
    return selected_values


def plot_selected_values_with_distance(values, reference, days, n_figure):
    new_selected_values = select_values_with_distance(values, reference)
    plt.figure(n_figure)
    n_figure += 1
    plot_chronology(np.matrix(new_selected_values, dtype=np.int32), days, 0, 28)
    return n_figure


def detect_with_distance_one(values, gmm, components, days, component_number):
    n_figure = 0
    for k in range(0, component_number):
        selected_values = select_values(values, components, k)
        if np.size(selected_values, 0) > 0:
            hist, edges = compute_distance_histogram(selected_values)
            if float(max(hist)) / float(np.size(values, 0)) > 0.05 or np.size(hist) == 1:
                index = np.argmax(hist)

                print(k, gmm.means_[k][0], edges[index], max(hist), np.size(selected_values, 0))

                plot_selected_values_with_distance(selected_values, edges[index], days, n_figure)
    return n_figure


def detect_with_distance_two(values, gmm, components, days, component_number):
    n_figure = 0
    for k1 in range(0, component_number):
        for k2 in range(0, component_number):
            if k1 != k2:
                selected_values = select_values_multi(values, components, [k1, k2])
                if np.size(selected_values, 0) > 0:
                    hist, edges = compute_distance_histogram(selected_values)
                    if float(max(hist)) / float(np.size(values, 0)) > 0.05:
                        index = np.argmax(hist)

                        print(k1, k2, gmm.means_[k1][0], gmm.means_[k2][0], edges[index], max(hist), np.size(selected_values, 0))

                        n_figure = plot_selected_values_with_distance(selected_values, edges[index], days, n_figure)
    return n_figure


def main():
    # files = ['2', '3', '4', '16', '23', 30', '35', '37', '43']
    file = '3'
    component_number = 8

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 38.4  # width = 3840 pixels
    fig_size[1] = 32  # height = 3200 pixels

    values, max_values, days = read(file)
    gmm = build(values, component_number)
    components = select(values, gmm, component_number)

    n_figure = detect_with_distance_one(values, gmm, components, days, component_number)

    # plt.figure(n_figure + 1)
    # compute_distance_one(values, gmm, components, component_number, plot_one)
    #
    # plt.figure(n_figure + 2)
    # compute_distance_two(values, gmm, components, component_number, plot_two)
    #
    # fig_size[0] = 38.4  # width = 3840 pixels
    # fig_size[1] = 6.4  # height = 640 pixels
    # plt.figure(n_figure + 3)
    # compute_distance_global(values, plot_global)

    plt.show()


if __name__ == "__main__":
    main()
